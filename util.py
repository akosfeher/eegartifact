#!/usr/bin/env python3

import numpy as np
from numpy import array
import mne
from numpy.random.mtrand import shuffle
import random
from PIL import Image
import os

def save4sec_20chanels(hdl):
    matrixAll = np.empty(shape= [7620,996,20], dtype=np.float)
    dbuf = np.empty(996, dtype=np.float_)

    print(matrixAll[10])

    for winNum in range (0, 7619):
        print(winNum)
        for chanel in range(0, 19):

            hdl.rewind(chanel)
            hdl.fseek(chanel, winNum * 996, 0)
            hdl.readSamples(chanel, dbuf, 996)

            for i in range(996):
                matrixAll[winNum, i, chanel] = dbuf[i]

    print(matrixAll[10])

    from numpy import save
    save('/opt/project/majlatiMatrix20.npy', matrixAll)

def bandpower(data, sf, band, method ='welch', window_sec =None, relative =False):
    """Compute the average power of the signal x in a specific frequency band.

    Requires MNE-Python >= 0.14.

    Parameters
    ----------
    data : 1d-array
      Input signal in the time-domain.
    sf : float
      Sampling frequency of the data.
    band : list
      Lower and upper frequencies of the band of interest.
    method : string
      Periodogram method: 'welch' or 'multitaper'
    window_sec : float
      Length of each window in seconds. Useful only if method == 'welch'.
      If None, window_sec = (1 / min(band)) * 2.
    relative : boolean
      If True, return the relative power (= divided by the total power of the signal).
      If False (default), return the absolute power.

    Return
    ------
    bp : float
      Absolute or relative band power.
    """

    #print('bandpower method: %s' % method )
    #print(method)
    from scipy.signal import welch
    from scipy.integrate import simps
    from mne.time_frequency import psd_array_multitaper

    band = np.asarray(band)
    low, high = band

    win = 4 * sf

    freqs, psd = welch(data, sf, nperseg=win)
    # Compute the modified periodogram (Welch)
    if method == 'welch':
        if window_sec is not None:
            nperseg = window_sec * sf
        else:
            nperseg = (2 / low) * sf

        freqs, psd = welch(data, sf, nperseg=nperseg)

    elif method == 'multitaper':
        psd, freqs = psd_array_multitaper(data, sf, adaptive=True,
                                          normalization='full', verbose=0)

    #    print('multitaper')

    # Frequency resolution
    freq_res = freqs[1] - freqs[0]

    # Find index of band in frequency vector
    idx_band = np.logical_and(freqs >= low, freqs <= high)

    # Integral approximation of the spectrum using parabola (Simpson's rule)
    bp = simps(psd[idx_band], dx=freq_res)

    if relative:
        bp /= simps(psd, dx=freq_res)
    return bp

def read_art(filename):
    with open(filename,'rb') as input:
        edfFileName= input.read(80).decode("utf-8")
        hypnoFileName = input.read(80).decode("utf-8").strip()

        ArtifactWindowLength= int(input.read(8).decode("utf-8"))
        ActArtifactWindow= int(input.read(8).decode("utf-8"))
        NumberOfArtifactWindows= int(input.read(8).decode("utf-8"))

        binary= input.read()

        print(edfFileName)
        print(hypnoFileName)

        print(ArtifactWindowLength)

        print(ActArtifactWindow)

        print(NumberOfArtifactWindows)

        sum= 0
        for i in range(0, NumberOfArtifactWindows):
            if i > 1000 and i < 1010:
               print('i=%d = %f' % (i, binary[i]))
            if binary[i] == 2:
                sum= sum + 1

        print(sum)
    return hypnoFileName, NumberOfArtifactWindows, binary

def read_hypno(hypnoFileName):
    with open(hypnoFileName, 'rb') as input:
        version = input.read(16).decode("utf-8")
        edfFileNameFromHyp = input.read(80).decode("utf-8")

        edfConst = input.read(8).decode("utf-8")

        numOfEpoch = int(input.read(8).decode("utf-8"))
        measuresInOneEpoch = int(input.read(8).decode("utf-8"))

        rest = input.read(166)

        actPos = int.from_bytes(input.read(2), byteorder='little')
        numOfEpochsFromBin = int.from_bytes(input.read(2), byteorder='little')
        input.read(2)
        epochLengthInSec = int.from_bytes(input.read(2), byteorder='little')
        measuresInOneEpochFromBin = int.from_bytes(input.read(2), byteorder='little')
        input.read(4)
        numOfEpochPlusOne = int.from_bytes(input.read(2), byteorder='little')
        input.read(8)

        cls = np.empty([numOfEpoch], dtype = np.int)
        classes  = []
        for inde in range(numOfEpoch):
            val = int.from_bytes(input.read(6), byteorder="little")
            cls[inde] = val
            ccc = [1,0,0,0,0,0,0,0]
            if(val == 1):
                ccc = [0,1,0,0,0,0,0,0]
            if(val == 2):
                ccc = [0,0,1,0,0,0,0,0]
            if(val == 3):
                ccc = [0,0,0,1,0,0,0,0]
            if(val == 4):
                ccc = [0,0,0,0,1,0,0,0]
            if(val == 5):
                ccc = [0,0,0,0,0,1,0,0]
            if(val == 6):
                ccc = [0,0,0,0,0,0,1,0]
            if(val == 7):
                ccc = [0,0,0,0,0,0,0,1]

            classes.append(ccc)

    return cls, classes

def label_img(name, binary):
  index = int(name[2:])
  return binary[index]

IMG_SIZE = 300

DIR = '/opt/project/artifBandPics'

def load_training_test_data(binary):
    train_data = []
    test_data = []
    for img in os.listdir(DIR):
        file_parts = img.split('.')
        ext = ''
        if len(file_parts) > 1:
            ext = file_parts[1]
        if ext == 'png':
            imgName = img.split('.')[0]  # converts 'p_123.npg' --> 'p_123'
            label = label_img(imgName, binary)
            path = os.path.join(DIR, img)
            img = Image.open(path)
            #img = img.convert('L')
            #img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
        if random.randint(1,100) < 66:
            train_data.append([np.array(img), label])
        else:
            test_data.append([np.array(img), label])

    shuffle(train_data)
    shuffle(test_data)

    return train_data, test_data

from numpy import load

def load_training_test_data_file(binary):
    train_data = []
    test_data = []

    matrixAllFlat = load('/opt/project/majlatiMatrixFlat.npy')
    for ind in range(len(matrixAllFlat)):
        label = 1#[1,0]
        if(binary[ind] == 2):
            label = 0#[0,1]
        if random.randint(1,100) < 66:
            train_data.append([matrixAllFlat[ind], label])
        else:
            test_data.append([matrixAllFlat[ind], label])

    shuffle(train_data)
    shuffle(test_data)

    return train_data, test_data

def load_training_test_data_file_classes(classes):
    train_data = []
    test_data = []

    matrixAllFlat = load('/opt/project/majlatiMatrixFlat.npy')
    print('matrixAllFlat.len: %d  classes len: %d' % (len(matrixAllFlat), len(classes)))
    for ind in range(len(matrixAllFlat)):
        label = classes[ind // 5]
        if random.randint(1,100) < 66:
            train_data.append([matrixAllFlat[ind], label])
        else:
            test_data.append([matrixAllFlat[ind], label])

    shuffle(train_data)
    shuffle(test_data)

    return train_data, test_data

def load_training_test_plain(binary):
    train_data = []
    test_data = []

    matrixAll = load('/opt/project/majlatiMatrix20.npy')
    print(matrixAll.shape)

    #np.moveaxis(matrixAll, 1, 4)

    #print('after swapaxes')
    #print(matrixAll.shape)
    for ind in range(len(matrixAll)):
        print(ind)
        chs = np.empty(shape= [20,996], dtype=np.float)
        for c in range(20):
            for sample in range(996):
                chs[c][sample] = matrixAll[ind][sample][c]
        label = 0#[1,0]
        if(binary[ind] == 2):
            label = 1#[0,1]
        if random.randint(1,100) < 666:
            train_data.append([chs, label])
        else:
            test_data.append([chs, label])

    #shuffle(train_data)
    #shuffle(test_data)

    return train_data, test_data

def generate_slice(act_type='Sl2All', val=False):
    """
    input target_value
    Sl:0
    Im:1
    Mem:2
    """

    assert act_type in {'Sl2All', 'Sl2Im', 'Sl2Mem', 'Im2Mem'}, 'act_type should be Sl2All or Sl2Im, Sl2Mem, Im2Mem'
    if act_type == 'Sl2All':
        type_name = np.random.choice(['Sl_trials', 'Im_trials', 'Mem_trials'], p=[0.46, 0.27, 0.27])
    elif act_type == 'Sl2Im':
        type_name = np.random.choice(['Sl_trials', 'Im_trials'])
    elif act_type == 'Sl2Mem':
        type_name = np.random.choice(['Sl_trials', 'Mem_trials'])
    else:
        type_name = np.random.choice(['Im_trials', 'Mem_trials'])

    person_id = np.random.choice(list(All_trials[type_name].keys()))
    if val:
        trial_id = np.random.choice(val_ids[type_name][person_id])
    else:
        trial_id = np.random.choice(train_ids[type_name][person_id])

    X = All_trials[type_name][person_id]['data'][trial_id, :, :]
    y = All_trials[type_name][person_id]['target']
    if act_type == 'Im2Mem':
        y = (lambda y: 0 if y == 2 else 1)(y)
    else:
        y = (lambda y: 0 if y == 0 else 1)(y)

    return X, y, (type_name, person_id, trial_id)



def data_generator(batch_size, resample_lens=[512, 1024, 3480], act_type='Sl2All', val=False):
    while True:

        batch_x = []
        batch_y = []

        for i in range(0, batch_size):
            x, y, _ = generate_slice(act_type, val)
            batch_x.append(x)
            batch_y.append(y)

        y = np.array(batch_y)

        x_small = np.array([resample(i, resample_lens[0]) for i in batch_x])
        x_med = np.array([resample(i, resample_lens[1]) for i in batch_x])
        x = np.array([i for i in batch_x])
        yield ([x_small, x_med, x], y)
