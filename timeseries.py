#!/usr/bin/env python3

import sys
import numpy as np

from edfreader import EDFreader

#mode = 'READ_BAND'
mode = 'LOAD_DATA'

mode = 'MODEL_4'

mode = 'TEST_DRAW'

filename= sys.argv[1]
chanel= int(sys.argv[2])
winNum= int(sys.argv[3])
winNum= 1141

print(filename)
print('----------------------------')


import random
import matplotlib.pyplot as plt
from tqdm import tqdm
#import pandas as pd
import numpy as np
from scipy.signal import resample
import os
import pickle


#dg = data_generator(10, val=False)
#X,y = next(dg)
#print(x)
#print(y)

hdl = EDFreader(filename)

# once save the data
#from util import save4sec_20chanels
#save4sec_20chanels(hdl)

hdl.close()

sf = 249.

win_sec = 4

from util import bandpower
import matplotlib.pyplot as plt

dbuf = np.empty(996, dtype = np.float_)

matrix = np.empty(shape= [20,5], dtype = np.float)

from numpy import save

print('mode %s' % mode)

import os
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'


from util import read_art

hypnoFileName, NumberOfArtifactWindows, binary = read_art('MajlatiSzandi2.art')

#print(binary)
from util import read_hypno

#read_hypno(hypnoFileName)

def getData():
    from util import load_training_test_plain

    train_data, test_data = load_training_test_plain(binary)


    print(train_data[1175][1])
    plt.plot(train_data[1175][0]); plt.title('Title 1175 label:0')
    plt.savefig('fig1175.png')
    plt.clf()
    #plt.imshow(test_data[10][0], interpolation='none')

    #plt.show()

    IMG_SIZE = 300

    import tensorflow as tf
    import numpy as np

    #trainImages = np.array([i[0] for i in train_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    trainImages = np.array([i[0] for i in train_data])
    trainLabels = np.array([i[1] for i in train_data])



    #one_hot_encode = []

    #for x in range(len(trainLabels)):
    #    enc = [1,0]
    #    if trainLabels[x] == 2:
    #        enc = [0,1]
    #    one_hot_encode.append(enc)

    #trainLabels = one_hot_encode

    #testImages = np.array([i[0] for i in test_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    testImages = np.array([i[0] for i in test_data])
    testLabels = np.array([i[1] for i in test_data])

    #testLabels = tf.keras.utils.to_categorical(testLabels, 2)
    #testLabels[testLabels > 1] = 0
    return trainImages, trainLabels, testImages, testLabels


from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.layers.normalization import BatchNormalization

from tensorflow.keras.layers import Conv1D, Dense, Dropout, Input, Concatenate, GlobalMaxPooling1D
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import RMSprop

#this base model is one branch of the main model
#it takes a time series as an input, performs 1-D convolution, and returns it as an output ready for concatenation
def get_base_model(input_len, fsize):
    #the input is a time series of length n and width 19
    input_seq = Input(shape=(input_len, 20))
    #choose the number of convolution filters
    nb_filters = 10
    #1-D convolution and global max-pooling
    convolved = Conv1D(nb_filters, fsize, padding="same", activation="tanh")(input_seq)
    processed = GlobalMaxPooling1D()(convolved)
    #dense layer with dropout regularization
    compressed = Dense(50, activation="tanh")(processed)
    compressed = Dropout(0.3)(compressed)
    model = Model(inputs=input_seq, outputs=compressed)
    return model

bm = get_base_model(996, 20)

bm.summary()

#this is the main model
#it takes the original time series and its down-sampled versions as an input, and returns the result of classification as an output
def main_model(inputs_lens = [249, 498, 996], fsizes = [8,16,24]):
    #the inputs to the branches are the original time series, and its down-sampled versions
    input_smallseq = Input(shape=(inputs_lens[0], 20))
    input_medseq = Input(shape=(inputs_lens[1] , 20))
    input_origseq = Input(shape=(inputs_lens[2], 20))
    #the more down-sampled the time series, the shorter the corresponding filter
    base_net_small = get_base_model(inputs_lens[0], fsizes[0])
    base_net_med = get_base_model(inputs_lens[1], fsizes[1])
    base_net_original = get_base_model(inputs_lens[2], fsizes[2])
    embedding_small = base_net_small(input_smallseq)
    embedding_med = base_net_med(input_medseq)
    embedding_original = base_net_original(input_origseq)
    #concatenate all the outputs
    merged = Concatenate()([embedding_small, embedding_med, embedding_original])
    out = Dense(1, activation='sigmoid')(merged)
    model = Model(inputs=[input_smallseq, input_medseq, input_origseq], outputs=out)
    return model

my_model = main_model()

my_model.summary()

opt = RMSprop(lr=0.005, clipvalue=10**6)
my_model.compile(loss="binary_crossentropy", optimizer=opt, metrics=['accuracy'])

from tensorflow.keras.callbacks import EarlyStopping

VALIDATION_SPLIT = 0.2  # how much TRAIN is reserved for VALIDATION
#nb_epoch = 100000
nb_epoch = 100
earlyStopping = EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='auto')
samples_per_epoch = 10

trainImages, trainLabels, testImages, testLabels = getData()

x_small = np.array([resample(i, 249) for i in trainImages])
x_med = np.array([resample(i, 498) for i in trainImages])
x = np.array([i for i in trainImages])

print(x_small.shape)
print(x_med.shape)

t_small = np.array([resample(i, 249) for i in testImages])
t_med = np.array([resample(i, 498) for i in testImages])
t = np.array([i for i in testImages])

print(x_small[1174][1][:])

plt.plot(x_small[1174][1][:]); plt.title('Small 1174')
plt.savefig('small1174.png')
plt.clf()

plt.plot(x_med[1174][1][:]); plt.title('Med 1174')
plt.savefig('med1174.png')
plt.clf()
if mode == 'TEST_DRAW':
    exit(0)


#v = var = np.zeros((996,2,20),dtype=float)

my_model.fit([x_small, x_med, x], trainLabels, samples_per_epoch, epochs=nb_epoch,
                    callbacks=[earlyStopping], verbose=1,  validation_split=0.2)

#my_model.fit_generator(data_generator(batch_size=50, act_type = 'Sl2All', val=False), samples_per_epoch, epochs=nb_epoch,
#                    callbacks=[earlyStopping], verbose=1, validation_steps=100,
#                    validation_data=data_generator(batch_size=50, act_type = 'Sl2All', val=True))


te_small = np.array([resample(i, 249) for i in testImages])
te_med = np.array([resample(i, 498) for i in testImages])
te= np.array([i for i in testImages])

test_loss, test_acc = my_model.evaluate([te_small, te_med, te], testLabels)
print('Test accuracy:', test_acc)