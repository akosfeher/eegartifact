#!/usr/bin/env python3

import sys
import numpy as np

from edfreader import EDFreader

#mode = 'READ_BAND'
#mode = 'LOAD_DATA'

mode = 'MODEL_4'

filename= sys.argv[1]
chanel= int(sys.argv[2])
winNum= int(sys.argv[3])
winNum= 1141

print(filename)
print('----------------------------')

hdl = EDFreader(filename)

sf = 249.

win_sec = 4

from util import bandpower
import matplotlib.pyplot as plt

dbuf = np.empty(996, dtype = np.float_)

matrix = np.empty(shape= [20,5], dtype = np.float)

from numpy import save

print('mode %s' % mode)
if mode == 'READ_BAND':
    matrixAll = np.empty(shape= [7620,20,5])
    matrixAllFlat = np.empty(shape=[7620,100])
    print(matrixAllFlat[10])

    for winNum in range (0, 7619):
        print(winNum)
        for chanel in range(0, 19):

            hdl.rewind(chanel)
            hdl.fseek(chanel, winNum * 996, 0)
            hdl.readSamples(chanel, dbuf, 996)

            w_delta_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='weltch', relative =True)
            w_theta_rel = bandpower(data =dbuf, sf =sf, band =[4, 8], window_sec =win_sec, method ='weltch', relative =True)
            w_alpha_rel = bandpower(data=dbuf, sf=sf, band=[8, 14], window_sec=win_sec, method='weltch', relative=True)
            w_beta_rel = bandpower(data =dbuf, sf =sf, band =[14, 30], window_sec =win_sec, method ='weltch', relative =True)
            w_gamma_rel = bandpower(data =dbuf, sf =sf, band =[30, 150], window_sec =win_sec, method ='weltch', relative =True)

            # r = w_delta_rel * 256
            # g = w_th_alph_rel * 256
            # b = w_bet_gam_rel * 256
            flatInd = chanel * 5
            matrix[chanel, 0] = w_delta_rel
            matrix[chanel, 1] = w_theta_rel
            matrix[chanel, 2] = w_alpha_rel
            matrix[chanel, 3] = w_beta_rel
            matrix[chanel, 4] = w_gamma_rel

            matrixAll[winNum, chanel, 0] = w_delta_rel
            matrixAll[winNum, chanel, 1] = w_theta_rel
            matrixAll[winNum, chanel, 2] = w_alpha_rel
            matrixAll[winNum, chanel, 3] = w_beta_rel
            matrixAll[winNum, chanel, 4] = w_gamma_rel

            matrixAllFlat[winNum, flatInd + 0] = w_delta_rel
            matrixAllFlat[winNum, flatInd + 1] = w_theta_rel
            matrixAllFlat[winNum, flatInd + 2] = w_alpha_rel
            matrixAllFlat[winNum, flatInd + 3] = w_beta_rel
            matrixAllFlat[winNum, flatInd + 4] = w_gamma_rel

        # print(matrix)
        # plt.imshow(matrix, interpolation='none')
        # save('majlatiMatrix.npy', matrix)
        # pngName = 'p_' + str(winNum) + '.png'
        #plt.imsave(pngName, matrix)

    print(matrixAllFlat[10])

    save('/opt/project/majlatiMatrix.npy', matrixAll)
    save('/opt/project/majlatiMatrixFlat.npy', matrixAllFlat)

    hdl.close()
    exit(0)



#time = np.arange(dbuf.size) / sf
#print(time)
# Plot the signal
#fig, ax = plt.subplots(1, 1, figsize=(12, 4))
#plt.plot(time, dbuf, lw=1.5, color='k')
#plt.xlabel('Time (seconds)')
#plt.ylabel('Voltage')
#plt.xlim([time.min(), time.max()])
#plt.title('N3 sleep EEG data (F3)')

# plt.show()

#tp_delta_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='multitaper', relative =True)
#tp_th_alph_rel = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='multitaper', relative =True)
#tp_bet_gam_rel = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='multitaper', relative =True)

#print('delta %.3f th_alph %.3f bet_gam %.3f' % (tp_delta_rel,tp_th_alph_rel,tp_bet_gam_rel))

#w_delta_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='weltch', relative =True)
#w_th_alph_rel = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='weltch', relative =True)
#w_bet_gam_rel = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='weltch', relative =True)

#print('www delta %.3f th_alph %.3f bet_gam %.3f' % (w_delta_rel,w_th_alph_rel,w_bet_gam_rel))

#tp_delta_abs = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='multitaper', relative =False)
#tp_th_alph_abs = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='multitaper', relative =False)
#tp_bet_gam_abs = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='multitaper', relative =False)

#print('abs  delta %.3f th_alph %.3f bet_gam %.3f' % (tp_delta_abs,tp_th_alph_abs,tp_bet_gam_abs))

#w_delta_abs = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='weltch', relative =False)
#w_th_alph_abs = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='weltch', relative =False)
#w_bet_gam_abs = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='weltch', relative =False)

#print('abs   www delta %.3f th_alph %.3f bet_gam %.3f' % (w_delta_abs,w_th_alph_abs,w_bet_gam_abs))


from util import read_art

hypnoFileName, NumberOfArtifactWindows, binary = read_art('MajlatiSzandi2.art')

#print(binary)
from util import read_hypno

cls, classes  = read_hypno(hypnoFileName)


if mode == 'LOAD_DATA' or mode == 'MODEL_3' or mode == 'MODEL_4':
    from util import load_training_test_data_file

    train_data, test_data = load_training_test_data_file(binary)


#print(test_data[10])

#plt.imshow(test_data[10][0], interpolation='none')

#plt.show()

    IMG_SIZE = 300

    import tensorflow as tf

    #trainImages = np.array([i[0] for i in train_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    trainImages = np.array([i[0] for i in train_data])
    trainLabels = np.array([i[1] for i in train_data])

    import numpy as np

    #one_hot_encode = []

    #for x in range(len(trainLabels)):
    #    enc = [1,0]
    #    if trainLabels[x] == 2:
    #        enc = [0,1]
    #    one_hot_encode.append(enc)

    #trainLabels = one_hot_encode

    #trainLabels[trainLabels > 1] = 0
    for few in range(0, 200):
        print('trainLabels[%d]:' % few)
        print(trainLabels[few])

    #testImages = np.array([i[0] for i in test_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    testImages = np.array([i[0] for i in test_data])
    testLabels = np.array([i[1] for i in test_data])

    #testLabels = tf.keras.utils.to_categorical(testLabels, 2)
    #testLabels[testLabels > 1] = 0

if mode == 'MODEL_5':
    print('load data %s' % mode)
    from util import load_training_test_data_file_classes

    train_data, test_data = load_training_test_data_file_classes(classes)

    import tensorflow as tf

    #trainImages = np.array([i[0] for i in train_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    trainImages = np.array([i[0] for i in train_data])
    trainLabels = np.array([i[1] for i in train_data])

    import numpy as np

    #one_hot_encode = []

    #for x in range(len(trainLabels)):
    #    enc = [1,0]
    #    if trainLabels[x] == 2:
    #        enc = [0,1]
    #    one_hot_encode.append(enc)

    #trainLabels = one_hot_encode

    #trainLabels[trainLabels > 1] = 0
    for few in range(0, 2):
        print('trainLabels[%d]:' % few)
        print(trainLabels[few])
        print(classes[few // 5])

    #testImages = np.array([i[0] for i in test_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
    testImages = np.array([i[0] for i in test_data])
    testLabels = np.array([i[1] for i in test_data])

    #testLabels = tf.keras.utils.to_categorical(testLabels, 2)
    #testLabels[testLabels > 1] = 0


from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.layers.normalization import BatchNormalization
from tensorflow.keras.losses import mean_squared_error, categorical_crossentropy

import os
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

if mode == "OLD_MODEL":
    model = Sequential()
    model.add(Conv2D(32, kernel_size = (3, 3), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, 1)))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(BatchNormalization())
    model.add(Conv2D(64, kernel_size=(3,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(BatchNormalization())
    model.add(Conv2D(64, kernel_size=(3,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(BatchNormalization())
    model.add(Conv2D(96, kernel_size=(3,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(BatchNormalization())
    model.add(Conv2D(32, kernel_size=(3,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    #model.add(Dropout(0.3))
    model.add(Dense(2, activation = 'softmax'))

    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics = ['accuracy'])

    checkpoint_path = "training_1/cp.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    import tensorflow as tf
    # Create a callback that saves the model's weights
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     save_weights_only=True,
                                                     verbose=1)


    model.fit(trainImages, trainLabels, batch_size = 100, epochs = 15, callbacks=[cp_callback], verbose = 1)

    loss, acc = model.evaluate(testImages, testLabels, verbose = 0)
    print(acc * 100)
    print('exit %s' % mode)
    exit(0)

if mode == 'MODEL_2':
    model = keras.Sequential([
    keras.layers.Flatten(input_shape=(5, 4, 4)),
    keras.layers.Dense(16, activation=tf.nn.relu),
    keras.layers.Dense(16, activation=tf.nn.relu),
    keras.layers.Dense(1, activation=tf.nn.sigmoid),
        ])

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])

    model.fit(trainImages, trainLabels, epochs=50, batch_size=1)
    test_loss, test_acc = model.evaluate(testImages, testLabels)
    print('Test accuracy:', test_acc)
    print('exit %s' % mode)
    exit(0)

if mode == 'MODEL_3':
    print('Start model_3')
    RESHAPED = 100
    EPOCHS = 50
    BATCH_SIZE = 128
    VERBOSE = 1
    NB_CLASSES = 2  # number of outputs = number of digits
    N_HIDDEN = 128
    VALIDATION_SPLIT = 0.2  # how much TRAIN is reserved for VALIDATION

    import tensorflow as tf
    # Build the model.
    model = tf.keras.models.Sequential()

    model.add(keras.layers.Dense(NB_CLASSES,
                                     input_shape=(RESHAPED,),
                                     name='dense_layer',
                                     activation='softmax'))

    # Compiling the model.
    model.compile(optimizer='SGD',
                        loss='categorical_crossentropy',
                        metrics=['accuracy'])

    # Training the model.
    model.fit(trainImages, trainLabels,
                  batch_size=BATCH_SIZE, epochs=EPOCHS,
                  verbose=VERBOSE, validation_split=VALIDATION_SPLIT)

    # evaluate the model
    test_loss, test_acc = model.evaluate(testImages, testLabels)
    print('Test accuracy:', test_acc)
    print('exit %s' % mode)
    exit(0)

ARTIF =  0
OK = 1

C_TP = 0.1
C_FP = 0.1
C_FN = 0.9
C_TN = 0.1

@tf.function
def weighted_cost(act, pred):
    print(act, pred, act[0], pred[0])

    ret = tf.cond(tf.equal(pred[0], OK) and tf.equal(act[0], OK), lambda: C_TP * categorical_crossentropy(act, pred), lambda : 0.1)
    ret = tf.cond(tf.equal(pred[0], OK) and tf.equal(act[0], ARTIF), lambda: C_FP * categorical_crossentropy(act, pred), lambda : 0.1)
    ret = tf.cond(tf.equal(pred[0], ARTIF) and tf.equal(act[0], OK), lambda: C_FN * categorical_crossentropy(act, pred), lambda : 0.1)
    ret = tf.cond(tf.equal(pred[0], ARTIF) and tf.equal(act[0], ARTIF), lambda: C_TN * categorical_crossentropy(act, pred), lambda : 0.1)

    return ret
    #if pred==OK and act==OK:
    #    return C_TP * categorical_crossentropy(act, pred)
    #if pred==OK and act==ARTIF:
    #    return C_FP * categorical_crossentropy(act, pred)
    #if pred==ARTIF and act==OK:
    #    return C_FN * categorical_crossentropy(act, pred)
    #if pred==ARTIF and act==ARTIF:
    #    return C_TN * categorical_crossentropy(act, pred)

from sklearn.metrics import classification_report

if mode == 'MODEL_4':
    print('Start %s' % mode)
    RESHAPED = 100
    EPOCHS = 20
    BATCH_SIZE = 10
    VERBOSE = 1
    NB_CLASSES = 2  # number of outputs = number of digits
    N_HIDDEN = 16
    VALIDATION_SPLIT = 0.2  # how much TRAIN is reserved for VALIDATION
    DROPOUT = 0.1

    import tensorflow as tf

    # Build the model.
    model = tf.keras.models.Sequential()
    model.add(keras.layers.Dense(N_HIDDEN,
                                 input_shape=(RESHAPED,),
                                 name='dense_layer', activation='relu'))
    model.add(keras.layers.Dropout(DROPOUT))
    model.add(keras.layers.Dense(N_HIDDEN,
                                 name='dense_layer_2', activation='relu'))
    model.add(keras.layers.Dropout(DROPOUT))
    model.add(keras.layers.Dense(NB_CLASSES,
                                 name='dense_layer_3', activation='softmax'))    # Summary of the model.
    model.summary()
    # Compiling the model.
    #weights = [0.1, 0.9]
    model.compile(optimizer='Adam',
                  loss='sparse_categorical_crossentropy',
                  #loss_weights=weights,
                  metrics=['accuracy'])
    cweights = { 0:10.1, 1:0.1}
    # Training the model.
    model.fit(trainImages, trainLabels,
              batch_size=BATCH_SIZE, epochs=EPOCHS, class_weight=cweights,
              verbose=VERBOSE, validation_split=VALIDATION_SPLIT)
    # Evaluating the model.
    test_loss, test_acc = model.evaluate(testImages, testLabels)
    print('exit %s' % mode)
    print('Test accuracy:', test_acc)

    #test = np.argmax(testLabels, axis=1)
    predTuti = model.predict_classes(testImages)


    print(classification_report(testLabels,predTuti))

    falsalarm = 0
    missed = 0
    artifact = 0
    ok = 0
    for ii in range(len(testLabels)):
        if testLabels[ii] != predTuti[ii]:
            #print('test: %d  - pred %d' % (testLabels[ii], pred[ii]))
            if testLabels[ii] == 0:
                falsalarm += 1
            else:
                missed += 1
        else:
            if testLabels[ii] == 0:
                artifact += 1
            else:
                ok += 1


    all = falsalarm + missed
    print('all error: %d  false alarm: %d  missed: %d  artifacts: %d  OK: %d' % (all,falsalarm, missed, artifact, ok))


    cweights = { 0:0.5, 1:0.5}
    # Training the model.
    model.fit(trainImages, trainLabels,
              batch_size=BATCH_SIZE, epochs=EPOCHS, class_weight=cweights,
              verbose=VERBOSE, validation_split=VALIDATION_SPLIT)
    # Evaluating the model.
    test_loss, test_acc = model.evaluate(testImages, testLabels)
    print('exit %s' % mode)
    print('Test accuracy:', test_acc)

    #test = np.argmax(testLabels, axis=1)
    predRev = model.predict_classes(testImages)


    print(classification_report(testLabels,predRev))

    falsalarm = 0
    missed = 0
    artifact = 0
    ok = 0
    q = 0
    for ii in range(len(testLabels)):
        if testLabels[ii] != predRev[ii]:
            #print('test: %d  - pred %d' % (testLabels[ii], pred[ii]))
            if testLabels[ii] == 0:
                falsalarm += 1
            else:
                missed += 1
        else:
            if testLabels[ii] == 0:
                artifact += 1
            else:
                ok += 1
        if predTuti[ii] == 1 and predRev[ii] == 0:
            q += 1
            #print(ii)

    all = falsalarm + missed
    print('------ all error: %d  false alarm: %d  missed: %d  artifacts: %d  OK: %d  questionable: %d' % (all,falsalarm, missed, artifact, ok, q))



    exit(0)

if mode == 'MODEL_5':
    print('Start %s' % mode)
    RESHAPED = 100
    EPOCHS = 20
    BATCH_SIZE = 10
    VERBOSE = 1
    NB_CLASSES = 8  # number of outputs = number of digits
    N_HIDDEN = 64
    VALIDATION_SPLIT = 0.2  # how much TRAIN is reserved for VALIDATION
    DROPOUT = 0.1

    import tensorflow as tf

    # Build the model.
    model = tf.keras.models.Sequential()
    model.add(keras.layers.Dense(N_HIDDEN,
                                 input_shape=(RESHAPED,),
                                 name='dense_layer', activation='relu'))
    model.add(keras.layers.Dropout(DROPOUT))
    model.add(keras.layers.Dense(N_HIDDEN,
                                 name='dense_layer_2', activation='relu'))
    model.add(keras.layers.Dropout(DROPOUT))
    model.add(keras.layers.Dense(N_HIDDEN,
                                 name='dense_layer_2a', activation='relu'))
    model.add(keras.layers.Dropout(DROPOUT))
    model.add(keras.layers.Dense(NB_CLASSES,
                                 name='dense_layer_3', activation='softmax'))    # Summary of the model.
    model.summary()
    # Compiling the model.
    model.compile(optimizer='Adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # Training the model.
    model.fit(trainImages, trainLabels,
              batch_size=BATCH_SIZE, epochs=EPOCHS,
              verbose=VERBOSE, validation_split=VALIDATION_SPLIT)
    # Evaluating the model.
    test_loss, test_acc = model.evaluate(testImages, testLabels)
    print('Test accuracy:', test_acc)

    test = np.argmax(testLabels, axis=1)
    pred = model.predict_classes(testImages)

    print(classification_report(test,pred))

    for ii in range(len(test)):
        if test[ii] != pred[ii]:
            print('test: %d  - pred %d' % (test[ii], pred[ii]))


    print(test)
    print(pred)

    print('exit %s' % mode)
    exit(0)