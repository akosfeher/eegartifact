#!/usr/bin/env python3

import sys
import numpy as np
from edfreader import EDFreader

import tensorflow as tf

print(tf.__version__)

gpu = tf.config.list_physical_devices('GPU')

print(gpu)

if sys.version_info[0] != 3 or sys.version_info[1] < 5:
  print("Must be using Python version >= 3.5.0")
  sys.exit()

if np.__version__ < "1.17.0":
  print("Must be using NumPy version >= 1.17.0")
  sys.exit()

if len(sys.argv) != 2:
  print("usage: test_edflib.py <filename>\n")
  sys.exit()

hdl = EDFreader(sys.argv[1])

print("\nStartdate: %02d-%02d-%04d" %(hdl.getStartDateDay(), hdl.getStartDateMonth(), hdl.getStartDateYear()))
print("Starttime: %02d:%02d:%02d" %(hdl.getStartTimeHour(), hdl.getStartTimeMinute(), hdl.getStartTimeSecond()))
filetype = hdl.getFileType()
if (filetype == hdl.EDFLIB_FILETYPE_EDF) or (filetype == hdl.EDFLIB_FILETYPE_BDF):
  print("Patient: %s" %(hdl.getPatient()))
  print("Recording: %s" %(hdl.getRecording()))
else:
  print("Patient code: %s" %(hdl.getPatientCode()))
  print("Gender: %s" %(hdl.getPatientGender()))
  print("Birthdate: %s" %(hdl.getPatientBirthDate()))
  print("Patient name: %s" %(hdl.getPatientName()))
  print("Patient additional: %s" %(hdl.getPatientAdditional()))
  print("Admin. code: %s" %(hdl.getAdministrationCode()))
  print("Technician: %s" %(hdl.getTechnician()))
  print("Equipment: %s" %(hdl.getEquipment()))
  print("Recording additional: %s" %(hdl.getRecordingAdditional()))
print("Reserved: %s" %(hdl.getReserved()))
edfsignals = hdl.getNumSignals()
print("Number of signals: %d" %(hdl.getNumSignals()))
print("Number of datarecords: %d" %(hdl.getNumDataRecords()))
print("Datarecord duration: %f" %(hdl.getLongDataRecordDuration() / 10000000.0))

n = edfsignals
if n > 3:
  n = 3

for i in range(0, n):
  print("\nSignal: %s" %(hdl.getSignalLabel(i)))
  print("Samplefrequency: %f Hz" %(hdl.getSampleFrequency(i)))
  print("Transducer: %s" %(hdl.getTransducer(i)))
  print("Physical dimension: %s" %(hdl.getPhysicalDimension(i)))
  print("Physical minimum: %f" %(hdl.getPhysicalMinimum(i)))
  print("Physical maximum: %f" %(hdl.getPhysicalMaximum(i)))
  print("Digital minimum: %d" %(hdl.getDigitalMinimum(i)))
  print("Digital maximum: %d" %(hdl.getDigitalMaximum(i)))
  print("Prefilter: %s" %(hdl.getPreFilter(i)))
  print("Samples per datarecord: %d" %(hdl.getSampelsPerDataRecord(i)))
  print("Total samples in file: %d" %(hdl.getTotalSamples(i)))
  print("Reserved: %s" %(hdl.getSignalReserved(i)))


n = len(hdl.annotationslist)

print("\nannotations in file: %d" %(n))

if n > 10:
  n = 10

for i in range(0, n):
  print("annotation: onset: %d:%02d:%02.3f    description: %s    duration: %d" %(\
        (hdl.annotationslist[i].onset / 10000000) / 3600, \
        ((hdl.annotationslist[i].onset / 10000000) % 3600) / 60, \
        (hdl.annotationslist[i].onset / 10000000) % 60, \
        hdl.annotationslist[i].description, \
        hdl.annotationslist[i].duration))

ibuf = np.empty(100, dtype = np.int32)
dbuf = np.empty(996, dtype = np.float_)

hdl.rewind(0)
hdl.readSamples(0, ibuf, 100)
hdl.rewind(0)
hdl.readSamples(0, dbuf, 996)

hdl.close()


for i in range(0, 100):
  print("buf[% 3d]:   %+8d   %+ 9.3f" %(i, ibuf[i], dbuf[i]))


import matplotlib.pyplot as plt
import seaborn as sns
sns.set(font_scale=1.2)

# Define sampling frequency and time vector
sf = 249.
time = np.arange(dbuf.size) / sf
print(time)
# Plot the signal
fig, ax = plt.subplots(1, 1, figsize=(12, 4))
plt.plot(time, dbuf, lw=1.5, color='k')
plt.xlabel('Time (seconds)')
plt.ylabel('Voltage')
plt.xlim([time.min(), time.max()])
plt.title('N3 sleep EEG data (F3)')

plt.show()

sns.despine()

from scipy import signal

# Define window length (4 seconds)
win = 4 * sf
freqs, psd = signal.welch(dbuf, sf, nperseg=win)

# Plot the power spectrum
sns.set(font_scale=1.2, style='white')
plt.figure(figsize=(8, 4))
plt.plot(freqs, psd, color='k', lw=2)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power spectral density (V^2 / Hz)')
plt.ylim([0, psd.max() * 1.1])
plt.title("Welch's periodogram")
plt.xlim([0, freqs.max()])

plt.show()

sns.despine()


# Define delta lower and upper limits
low, high = 0.5, 4

# Find intersecting values in frequency vector
idx_delta = np.logical_and(freqs >= low, freqs <= high)

# Plot the power spectral density and fill the delta area
plt.figure(figsize=(7, 4))
plt.plot(freqs, psd, lw=2, color='k')
plt.fill_between(freqs, psd, where=idx_delta, color='skyblue')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power spectral density (uV^2 / Hz)')
plt.xlim([0, 10])
plt.ylim([0, psd.max() * 1.1])
plt.title("Welch's periodogram")

plt.show()

sns.despine()

from scipy.integrate import simps

# Frequency resolution
freq_res = freqs[1] - freqs[0]  # = 1 / 4 = 0.25

# Compute the absolute power by approximating the area under the curve
delta_power = simps(psd[idx_delta], dx=freq_res)
print('Absolute delta power: %.3f uV^2' % delta_power)

# Relative delta power (expressed as a percentage of total power)
total_power = simps(psd, dx=freq_res)
delta_rel_power = delta_power / total_power
print('Relative delta power: %.3f' % delta_rel_power)

from util import bandpower

win_sec = 4

db_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, relative =True) / bandpower(data =dbuf, sf =sf, band =[12, 30], window_sec = win_sec, relative =True)

print('Delta/beta ratio (relative): %.3f' % db_rel)

db_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='multitaper', relative =True) / bandpower(data =dbuf, sf =sf, band =[12, 30], window_sec =win_sec, method ='multitaper', relative =True)

print('Delta/beta ratio multitaper (relative): %.3f' % db_rel)

tp_delta_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='multitaper', relative =True)
tp_th_alph_rel = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='multitaper', relative =True)
tp_bet_gam_rel = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='multitaper', relative =True)

print('delta %.3f th_alph %.3f bet_gam %.3f' % (tp_delta_rel,tp_th_alph_rel,tp_bet_gam_rel))

w_delta_rel = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='weltch', relative =True)
w_th_alph_rel = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='weltch', relative =True)
w_bet_gam_rel = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='weltch', relative =True)

print('www delta %.3f th_alph %.3f bet_gam %.3f' % (w_delta_rel,w_th_alph_rel,w_bet_gam_rel))

tp_delta_abs = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='multitaper', relative =False)
tp_th_alph_abs = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='multitaper', relative =False)
tp_bet_gam_abs = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='multitaper', relative =False)

print('abs  delta %.3f th_alph %.3f bet_gam %.3f' % (tp_delta_abs,tp_th_alph_abs,tp_bet_gam_abs))

w_delta_abs = bandpower(data =dbuf, sf =sf, band =[0.5, 4], window_sec =win_sec, method ='weltch', relative =False)
w_th_alph_abs = bandpower(data =dbuf, sf =sf, band =[4, 12], window_sec =win_sec, method ='weltch', relative =False)
w_bet_gam_abs = bandpower(data =dbuf, sf =sf, band =[12, 200], window_sec =win_sec, method ='weltch', relative =False)

print('abs   www delta %.3f th_alph %.3f bet_gam %.3f' % (w_delta_abs,w_th_alph_abs,w_bet_gam_abs))


#fft = tf.signal.fft(dbuf)

#power_spectrum = np.sqrt(fft)
#sampling_rate = 249

#frequency = np.linspace(0, sampling_rate/2, len(power_spectrum))

#plt.plot(frequency, power_spectrum)

#plt.show()




